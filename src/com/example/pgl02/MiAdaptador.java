package com.example.pgl02;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

public class MiAdaptador extends ArrayAdapter<Nodo> implements OnClickListener {

	public static ArrayList<Nodo> nodos = new ArrayList<Nodo>();

	public MiAdaptador(Context contexto, int elementosListaResourceId,
			ArrayList<Nodo> nodos) {
		super(contexto, elementosListaResourceId, nodos);
		this.nodos = new ArrayList<Nodo>();
		this.nodos.addAll(nodos);
	}

	private class ViewCheck {
		CheckBox chkPais;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return nodos.size();
	}

	@Override
	public Nodo getItem(int posicion) {
		// TODO Auto-generated method stub
		return nodos.get(posicion);
	}

	@Override
	public long getItemId(int posicion) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int posicion, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewCheck viewCheck = null;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) super.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.elemento_pais, null);

			viewCheck = new ViewCheck();
			viewCheck.chkPais = (CheckBox) convertView
					.findViewById(R.id.chkPais);
			convertView.setTag(viewCheck.chkPais);

			viewCheck.chkPais.setOnClickListener(this);
		} else
			viewCheck = (ViewCheck) convertView.getTag();

		Nodo nodo = nodos.get(posicion);
		viewCheck.chkPais.setText(nodo.pais);
		viewCheck.chkPais.setChecked(nodo.isSeleccionado());

		return convertView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		CheckBox cb = (CheckBox) v;
		Nodo nodo = new Nodo();
		nodo.setPais(cb.getText().toString());
		nodo.setSeleccionado(cb.isChecked());
		
		int i = 0;
		for (Nodo n : nodos) {
			if (n.getPais().equals(cb.getText().toString())) {
				nodos.set(i, nodo);
			}
			i++;
		}
	}

}
