package com.example.pgl02;

public class Nodo {
	public String pais;
	public boolean seleccionado = false;
	
	public Nodo(String pais, boolean seleccionado) {
		this.pais = pais;
		this.seleccionado = seleccionado;
	}
	
	public Nodo() { }

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public boolean isSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	
}