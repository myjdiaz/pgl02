package com.example.pgl02;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Paises extends Activity implements OnClickListener {

	private String[] continentes = { "Europa", "Africa", "Asia", "America",
	"Oceania" };

private TextView txvContinente;
	
	private ImageView imvPais1;
	private TextView txvPais1;

	private ImageView imvPais2;
	private TextView txvPais2;

	private ImageView imvPais3;
	private TextView txvPais3;

	private Button btnVolver;

	private boolean[] paises = new boolean[3];
	private int posicion;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.paises);

		btnVolver = (Button) findViewById(R.id.btnVolver2);
		btnVolver.setOnClickListener(this);

		Bundle bolsa = super.getIntent().getExtras();
		paises = bolsa.getBooleanArray("paises");
		posicion = bolsa.getInt("posicion");
		
		txvContinente = (TextView) findViewById(R.id.txvContinente_Paises);
		txvContinente.setText(this.continentes[this.posicion]);

		imvPais1 = (ImageView) findViewById(R.id.imwPais1);
		txvPais1 = (TextView) findViewById(R.id.txvPais1);

		imvPais2 = (ImageView) findViewById(R.id.imwPais2);
		txvPais2 = (TextView) findViewById(R.id.txvPais2);

		imvPais3 = (ImageView) findViewById(R.id.imvPais3);
		txvPais3 = (TextView) findViewById(R.id.txvPais3);

		cargarImagenes();

	}

	private void cargarImagen(boolean seleccionado, int indice) {

		if ((indice == 0) && (seleccionado)) {
			switch (posicion) {
			case 0:
				imvPais1.setImageResource(R.drawable.espana);
				txvPais1.setText(this.getResources().getString(
						R.string.tituloPaisEuropa0));
				break;
			case 1:
				imvPais1.setImageResource(R.drawable.angola);
				txvPais1.setText(this.getResources().getString(
						R.string.tituloPaisAfrica0));
				break;
			case 2:
				imvPais1.setImageResource(R.drawable.china);
				txvPais1.setText(this.getResources().getString(
						R.string.tituloPaisAsia0));
				break;
			case 3:
				imvPais1.setImageResource(R.drawable.argentina);
				txvPais1.setText(this.getResources().getString(
						R.string.tituloPaisAmerica0));
				break;
			case 4:
				imvPais1.setImageResource(R.drawable.australia);
				txvPais1.setText(this.getResources().getString(
						R.string.tituloPaisOceania0));
				break;
			}
		}

		if ((indice == 1) && (seleccionado)) {
			switch (posicion) {
			case 0:
				imvPais2.setImageResource(R.drawable.italia);
				txvPais2.setText(this.getResources().getString(
						R.string.tituloPaisEuropa1));
				break;
			case 1:
				imvPais2.setImageResource(R.drawable.kenya);
				txvPais2.setText(this.getResources().getString(
						R.string.tituloPaisAfrica1));
				break;
			case 2:
				imvPais2.setImageResource(R.drawable.india);
				txvPais2.setText(this.getResources().getString(
						R.string.tituloPaisAsia1));
				break;
			case 3:
				imvPais2.setImageResource(R.drawable.cuba);
				txvPais2.setText(this.getResources().getString(
						R.string.tituloPaisAmerica1));
				break;
			case 4:
				imvPais2.setImageResource(R.drawable.papua_new_guinea);
				txvPais2.setText(this.getResources().getString(
						R.string.tituloPaisOceania1));
				break;
			}
		}

		if ((indice == 2) && (seleccionado)) {
			switch (posicion) {
			case 0:
				imvPais3.setImageResource(R.drawable.reino_unido);
				txvPais3.setText(this.getResources().getString(
						R.string.tituloPaisEuropa2));
				break;
			case 1:
				imvPais3.setImageResource(R.drawable.sudan);
				txvPais3.setText(this.getResources().getString(
						R.string.tituloPaisAfrica2));
				break;
			case 2:
				imvPais3.setImageResource(R.drawable.mongolia);
				txvPais3.setText(this.getResources().getString(
						R.string.tituloPaisAsia2));
				break;
			case 3:
				imvPais3.setImageResource(R.drawable.mexico);
				txvPais3.setText(this.getResources().getString(
						R.string.tituloPaisAmerica2));
				break;
			case 4:
				imvPais3.setImageResource(R.drawable.vanuatu);
				txvPais3.setText(this.getResources().getString(
						R.string.tituloPaisOceania2));
				break;
			}
		}
	}

	private void cargarImagenes() {

		for (int i = 0; i < paises.length; i++)
			cargarImagen(paises[i], i);

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		finish();
	}

}
