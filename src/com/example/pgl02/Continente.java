package com.example.pgl02;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Continente extends Activity implements OnClickListener {

	private String[] continentes = { "Europa", "Africa", "Asia", "America",
			"Oceania" };
	private int posicionItemSeleccionado;

	private ArrayList<Nodo> nodos = new ArrayList<Nodo>();

	private TextView txvContinente;
	private ImageView imvImagenContinente;
	private TextView txvInfoContinente;
	private TextView txvTituloPaises;

	private MiAdaptador miAdaptador;
	private ListView listaPaises;

	private Button btnVolver;
	private Button btnVerPaises;

	private void cargarDatos() {

		switch (posicionItemSeleccionado) {
		case 0:
			Nodo miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisEuropa0);
			nodos.add(miNodo);
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisEuropa1);
			nodos.add(miNodo);
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisEuropa2);
			nodos.add(miNodo);
			imvImagenContinente.setImageResource(R.drawable.europa);
			txvInfoContinente.setText(this.getResources().getString(
					R.string.descripcionEuropa));
			txvTituloPaises.setText(this.getResources().getString(
					R.string.tituloPaisesEuropa));
			break;
		case 1:
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisAfrica0);
			nodos.add(miNodo);
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisAfrica1);
			nodos.add(miNodo);
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisAfrica2);
			nodos.add(miNodo);
			imvImagenContinente.setImageResource(R.drawable.africa);
			txvInfoContinente.setText(this.getResources().getString(
					R.string.descripcionAfrica));
			txvTituloPaises.setText(this.getResources().getString(
					R.string.tituloPaisesAfrica));
			break;
		case 2:
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisAsia0);
			nodos.add(miNodo);
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisAsia1);
			nodos.add(miNodo);
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisAsia2);
			nodos.add(miNodo);
			imvImagenContinente.setImageResource(R.drawable.asia);
			txvInfoContinente.setText(this.getResources().getString(
					R.string.descripcionAsia));
			txvTituloPaises.setText(this.getResources().getString(
					R.string.tituloPaisesAsia));
			break;
		case 3:
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisAmerica0);
			nodos.add(miNodo);
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisAmerica1);
			nodos.add(miNodo);
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisAmerica2);
			nodos.add(miNodo);
			imvImagenContinente.setImageResource(R.drawable.america);
			txvInfoContinente.setText(this.getResources().getString(
					R.string.descripcionAmerica));
			txvTituloPaises.setText(this.getResources().getString(
					R.string.tituloPaisesAmerica));
			break;
		case 4:
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisOceania0);
			nodos.add(miNodo);
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisOceania1);
			nodos.add(miNodo);
			miNodo = new Nodo();
			miNodo.pais = this.getResources().getString(
					R.string.tituloPaisOceania2);
			nodos.add(miNodo);
			imvImagenContinente.setImageResource(R.drawable.oceania);
			txvInfoContinente.setText(this.getResources().getString(
					R.string.descripcionOceania));
			txvTituloPaises.setText(this.getResources().getString(
					R.string.tituloPaisesOceania));
			break;
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.continente);

		Bundle bundle = super.getIntent().getExtras();
		posicionItemSeleccionado = bundle.getInt("posicion");

		btnVolver = (Button) findViewById(R.id.btnVolver);
		btnVolver.setOnClickListener(this);
		btnVerPaises = (Button) findViewById(R.id.btnVerPaises);
		btnVerPaises.setOnClickListener(this);

		txvContinente = (TextView) findViewById(R.id.txvContinente);
		imvImagenContinente = (ImageView) findViewById(R.id.imwContinente);
		txvInfoContinente = (TextView) findViewById(R.id.txvInfoContinente);
		txvTituloPaises = (TextView) findViewById(R.id.txvTituloPaises);

		txvContinente.setText(this.continentes[this.posicionItemSeleccionado]);
		listaPaises = (ListView) findViewById(R.id.lsvPaises);
		cargarDatos();
		miAdaptador = new MiAdaptador(this, R.layout.elemento_pais, this.nodos);
		listaPaises.setAdapter(miAdaptador);

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.btnVolver:
			finish();
			break;
		case R.id.btnVerPaises:
			Intent intent = new Intent(this, Paises.class);
			Bundle bolsa = new Bundle();

			boolean[] paises = new boolean[3];
			int i = 0;
			for (Nodo n : miAdaptador.nodos) {
				paises[i] = n.isSeleccionado();
				i++;
			}

			bolsa.putBooleanArray("paises", paises);
			bolsa.putInt("posicion", this.posicionItemSeleccionado);
			intent.putExtras(bolsa);
			super.startActivity(intent);
			break;
		}

	}

}
