package com.example.pgl02;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class PGL02 extends Activity implements OnItemClickListener,
		OnClickListener {

	private ListView listView;
	private View botonSalir;
	private String continentes[] = new String[] { "Europa", "Africa", "Asia",
			"America", "Oceania" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pgl02);

		listView = (ListView) findViewById(R.id.lsvContinentes);
		listView.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, continentes));
		listView.setOnItemClickListener(this);

		botonSalir = (View) findViewById(R.id.btnSalir);
		botonSalir.setOnClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		int itemPosicion = position;
		String itemValor = String.valueOf(listView
				.getItemIdAtPosition(position));

		Intent intent = new Intent(this, Continente.class);
		// intent.putExtra("valor", itemValor);
		Bundle bolsa = new Bundle();
		// bolsa.putString("item", itemValor);
		bolsa.putInt("posicion", position);
		intent.putExtras(bolsa);

		startActivity(intent);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		super.finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pgl02, menu);
		return true;
	}

}