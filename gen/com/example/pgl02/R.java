/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.pgl02;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int azulCabeceraPie=0x7f060001;
        public static final int azulFondo=0x7f060000;
        public static final int azulFuenteCabPie=0x7f060002;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int africa=0x7f020000;
        public static final int america=0x7f020001;
        public static final int angola=0x7f020002;
        public static final int argentina=0x7f020003;
        public static final int asia=0x7f020004;
        public static final int australia=0x7f020005;
        public static final int china=0x7f020006;
        public static final int cuba=0x7f020007;
        public static final int espana=0x7f020008;
        public static final int europa=0x7f020009;
        public static final int ic_launcher=0x7f02000a;
        public static final int india=0x7f02000b;
        public static final int italia=0x7f02000c;
        public static final int kenya=0x7f02000d;
        public static final int mexico=0x7f02000e;
        public static final int mongolia=0x7f02000f;
        public static final int oceania=0x7f020010;
        public static final int papua_new_guinea=0x7f020011;
        public static final int reino_unido=0x7f020012;
        public static final int sudan=0x7f020013;
        public static final int vanuatu=0x7f020014;
    }
    public static final class id {
        public static final int action_settings=0x7f090013;
        public static final int btnSalir=0x7f090012;
        public static final int btnVerPaises=0x7f090006;
        public static final int btnVolver=0x7f090005;
        public static final int btnVolver2=0x7f09000f;
        public static final int chkPais=0x7f090007;
        public static final int imvPais3=0x7f09000e;
        public static final int imwContinente=0x7f090001;
        public static final int imwPais1=0x7f09000a;
        public static final int imwPais2=0x7f09000c;
        public static final int lsvContinentes=0x7f090011;
        public static final int lsvPaises=0x7f090004;
        public static final int txvContinente=0x7f090000;
        public static final int txvContinente_Paises=0x7f090008;
        public static final int txvContinentes=0x7f090010;
        public static final int txvInfoContinente=0x7f090002;
        public static final int txvPais1=0x7f090009;
        public static final int txvPais2=0x7f09000b;
        public static final int txvPais3=0x7f09000d;
        public static final int txvTituloPaises=0x7f090003;
    }
    public static final class layout {
        public static final int continente=0x7f030000;
        public static final int elemento_pais=0x7f030001;
        public static final int paises=0x7f030002;
        public static final int pgl02=0x7f030003;
    }
    public static final class menu {
        public static final int pgl02=0x7f080000;
    }
    public static final class string {
        public static final int action_settings=0x7f050001;
        public static final int app_name=0x7f050000;
        public static final int btnSalir=0x7f050003;
        public static final int btnVerPaises=0x7f050005;
        public static final int btnVolver=0x7f050004;
        public static final int descripcionAfrica=0x7f050016;
        public static final int descripcionAmerica=0x7f050011;
        public static final int descripcionAsia=0x7f05000c;
        public static final int descripcionEuropa=0x7f050007;
        public static final int descripcionOceania=0x7f05001b;
        public static final int imagen_Continente=0x7f050006;
        public static final int texto_continentes=0x7f050002;
        public static final int tituloPaisAfrica0=0x7f050017;
        public static final int tituloPaisAfrica1=0x7f050018;
        public static final int tituloPaisAfrica2=0x7f050019;
        public static final int tituloPaisAmerica0=0x7f050012;
        public static final int tituloPaisAmerica1=0x7f050013;
        public static final int tituloPaisAmerica2=0x7f050014;
        public static final int tituloPaisAsia0=0x7f05000d;
        public static final int tituloPaisAsia1=0x7f05000e;
        public static final int tituloPaisAsia2=0x7f05000f;
        public static final int tituloPaisEuropa0=0x7f050008;
        public static final int tituloPaisEuropa1=0x7f050009;
        public static final int tituloPaisEuropa2=0x7f05000a;
        public static final int tituloPaisOceania0=0x7f05001c;
        public static final int tituloPaisOceania1=0x7f05001d;
        public static final int tituloPaisOceania2=0x7f05001e;
        public static final int tituloPaisesAfrica=0x7f05001a;
        public static final int tituloPaisesAmerica=0x7f050015;
        public static final int tituloPaisesAsia=0x7f050010;
        public static final int tituloPaisesEuropa=0x7f05000b;
        public static final int tituloPaisesOceania=0x7f05001f;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070001;
    }
}
